import { combineReducers } from 'redux';

import { addressReducer } from './AddressReducer';
import { productsReducer } from './ProductsReducer';
import { cartReducer } from './CartReducer';
import { categoryReducer } from './CategoryReducer';

export const Reducers = combineReducers({
  address: addressReducer,
  products: productsReducer,
  cart: cartReducer,
  categories: categoryReducer
});