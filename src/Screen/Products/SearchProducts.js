import React, { useState } from "react"
import { useQuery } from "@apollo/client"
import { withRouter } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"

import { receiveCategory } from "../../Actions/CategoryAction"
import { GET_CATEGORIES } from "../../queries"
import lupa from "../../images/lupa.svg"

const SearchProducts = ({ productsSearch }) => {
  const dispatch = useDispatch()

  const [category, setCategory] = useState("")
  const [textSearch, setTextSearch] = useState("")
  const categories = useSelector((state) => state.categories.categories)
  const pocSearch = useSelector((state) => state.address.poc)

  const { data } = useQuery(GET_CATEGORIES, {
    onCompleted: () => {
      dispatch(receiveCategory(data.allCategory))
    },
  })

  const selectCategory = (e) => {
    setCategory(e.target.value)

    setTextSearch("")

    productsSearch({
      variables: {
        id: pocSearch,
        categoryId: e.target.value,
      },
    })
  }

  const onSearch = (e) => {
    e.preventDefault()
    
    setCategory("")

    productsSearch({
      variables: {
        id: pocSearch,
        search: textSearch
      },
    })
  }

  return (
    <form
      onSubmit={onSearch}
      className="center-page box-filter display-flex justify-content-space-between product-filter"
    >
      <select
        className="filter-select"
        name="category"
        onChange={selectCategory}
        value={category}
      >
        <option value="">Todos</option>
        {categories.map(function (category, index) {
          return (
            <option key={index} value={category.id}>
              {category.title}
            </option>
          );
        })}
      </select>

      <section className="filter-input">
        <input
          type="text"
          className="text-input"
          placeholder="Pesquisar"
          name="search"
          value={textSearch}
          onChange={(e) => setTextSearch(e.target.value)}
        />
        <button>
          <img src={lupa} className="search-address-image" alt="Buscar" />
        </button>
      </section>
    </form>
  );
}

export default withRouter(SearchProducts)
