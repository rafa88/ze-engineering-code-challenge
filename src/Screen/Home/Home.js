import React, { useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";
import { useLazyQuery } from "@apollo/client";

import Header from "../../Component/Header/Header";
import Spinner from "../../Component/Spinner/Spinner";
import { GET_ADDRESS } from "../../queries";

import lupa from "../../images/lupa.svg";
import "./Home.css";
import { receiveAddress } from "../../Actions/AddressAction";

const Home = ({ history }) => {
  const autoCompleteRef = useRef(null);
  const [address, setAddress] = useState({});
  const dispatch = useDispatch();

  const [addressSearch, { loading, data }] = useLazyQuery(GET_ADDRESS, {
    onCompleted: () => {
      if (data && data.pocSearch.length > 0) {
        dispatch(
          receiveAddress({
            ...data.pocSearch[0],
            lat: address.geometry.location.lat(),
            lng: address.geometry.location.lng(),
          })
        );
        history.push(`produtos/${data.pocSearch[0].id}`);
      }
    },
  });

  function handleScriptLoad() {
    let autoComplete;
    autoComplete = new window.google.maps.places.Autocomplete(
      autoCompleteRef.current,
      { types: ["geocode"] }
    );
    autoComplete.addListener("place_changed", () => {
      const addressObject = autoComplete.getPlace();
      setAddress(addressObject);

      addressSearch({
        variables: {
          algorithm: "NEAREST",
          lat: addressObject.geometry.location.lat(),
          long: addressObject.geometry.location.lng(),
          now: new Date(),
        }
      });
    });
  }

  useEffect(() => {
    handleScriptLoad();
  });

  return (
    <div className="App">
      {loading && <Spinner />}
      <Header />

      <section className="search-address">
        <input
          type="text"
          className="text-input"
          ref={autoCompleteRef}
          placeholder="Ex. Rua Américo Brasiliense, São Paulo"
        />
        <button>
          <img
            src={lupa}
            className="search-address-image"
            alt="buscar endereço"
          />
        </button>
      </section>
    </div>
  );
};
export default withRouter(Home);
