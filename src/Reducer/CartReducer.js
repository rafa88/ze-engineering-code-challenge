import { ADD_PROD, REMOVE_PROD } from "../Actions/ActionTypes";

const initialState = 0;

export const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PROD:
      return action.value;
    case REMOVE_PROD:
      return action.value;
    default:
      return state;
  }
};
