import { FETCH_POC_RECEIVE } from "./ActionTypes";

export const receiveAddress = (address) => (dispatch) => {
  sessionStorage.setItem("pocSearch", address.id);
  sessionStorage.setItem(
    "address",
    JSON.stringify(address.address)
  );
  sessionStorage.setItem("lat", address.lat);
  sessionStorage.setItem("lng", address.lng);
  dispatch({
    type: FETCH_POC_RECEIVE,
    value: {
      pocSearch: address,
      lat: address.lat,
      lng: address.lng,
    },
  });
};
