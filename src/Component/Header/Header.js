import React from 'react';
import PropTypes from "prop-types";

import './Header.css';

const Header = ({address, total}) => {
  return (
    <header className="header display-flex align-items-center">
      {(address && address.address) &&
        <div className="center-page display-flex justify-content-flex-end header-address">
          <p>
            Endereço de entrega<br />
            <strong>{address.address.address1}, {address.address.number}</strong>
          </p>
          <p>
            Total:<br />
            <strong>{total.toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' })}</strong>
          </p>
        </div>
      }
      {(address && !address.address) &&
        <div className="center-page display-flex justify-content-flex-end header-user">
          <button className="header-user-btn">login</button>
        </div>
      }
    </header>
  )
}

Header.propTypes = {
  address: PropTypes.object,
  total: PropTypes.number
};
export default Header;

