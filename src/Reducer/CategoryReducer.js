import { FETCH_CATEGORY_RECEIVE } from "../Actions/ActionTypes";

const initialState = {
  categories: [],
};

export const categoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATEGORY_RECEIVE:
      return {
        ...state,
        categories: action.value,
      };
    default:
      return state;
  }
};
