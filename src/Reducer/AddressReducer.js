import { FETCH_POC_RECEIVE } from "../Actions/ActionTypes";

const initialState = {
  address: JSON.parse(sessionStorage.getItem('address')) || null,
  lat: sessionStorage.getItem('lat') || null,
  lng: sessionStorage.getItem('lng') || null,
  poc: sessionStorage.getItem('pocSearch') || null,
};

export const addressReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POC_RECEIVE:
      return {
        ...state,
        address: action.value.pocSearch.address,
        lat: action.value.lat,
        lng: action.value.lng,
        poc: action.value.pocSearch.id
      };
    default:
      return state;
  }
};
