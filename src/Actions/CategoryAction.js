import { FETCH_CATEGORY_RECEIVE } from "./ActionTypes";

export const receiveCategory = (data) => async (dispatch) => {
  dispatch({
    type: FETCH_CATEGORY_RECEIVE,
    value: data,
  });
};
