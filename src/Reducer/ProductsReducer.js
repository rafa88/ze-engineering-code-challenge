import { FETCH_PRODUTCS_RECEIVE } from "../Actions/ActionTypes";

const initialState = {
  products: [],
  poc: null,
};

export const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUTCS_RECEIVE:
      return {
        ...state,
        products: action.value.products,
        poc: action.value.poc
      };
    default:
      return state;
  }
};
